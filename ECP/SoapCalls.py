from requests import Session
from zeep.transports import Transport
from zeep import Client
from zeep.wsse.username import UsernameToken


class SoapCalls:

    session = Session()
    # Disables SSL certificate checking
    session.verify = False
    transport = Transport(session=session)

    wsdl_url = 'https://127.0.0.1:11003/ECP_MODULE/services/MadesEndpoint.wsdl'
    client = Client(
        wsdl_url,
        wsse=UsernameToken('admin', 'password'),
        transport=transport
    )

    @classmethod
    def send_message(cls, receiver_code, business_type, content):
        send_message_response = cls.client.service.SendMessage({
            'receiverCode': receiver_code,
            'businessType': business_type,
            'content': content,
        })
        return send_message_response

    @classmethod
    def receive_message(cls, business_type="*", download_message="false"):
        received_message_response = cls.client.service.ReceiveMessage(
            businessType=business_type,
            downloadMessage=download_message
        )
        return received_message_response

    @classmethod
    def confirm_received_message(cls, message_id):
        confirm_received_message_response = cls.client.service.ConfirmReceiveMessage(messageID=message_id)
        return confirm_received_message_response

    @classmethod
    def send_test_messages(cls, endpoint, business_type):
        response = SoapCalls.send_message(
            endpoint,
            business_type,
            'VGVzdGluaXMgdHh0IGZhaWxhcyAwCg'
        )

        response1 = SoapCalls.send_message(
            endpoint,
            business_type,
            'VGVzdGluaXMgdHh0IGZhaWxhcyAxCg'
        )

        response2 = SoapCalls.send_message(
            endpoint,
            business_type,
            'VGVzdGluaXMgdHh0IGZhaWxhcyAyCg'
        )

        print(response)
        print(response1)
        print(response2)

    @classmethod
    def receive_all_messages(cls, business_type, download_messages):
        remaining_message_count = 1

        while remaining_message_count > 0:
            receive_message_response = SoapCalls.receive_message(
                business_type=business_type,
                download_message=download_messages
            )
            print(receive_message_response)

            if receive_message_response['receivedMessage']:
                confirm_receive_message_response = SoapCalls.confirm_received_message(
                    receive_message_response['receivedMessage']['messageID'])

                if isinstance(confirm_receive_message_response, str):
                    print('Acknowledged receipt')
                else:
                    print('Error confirming receipt')

            remaining_message_count = receive_message_response['remainingMessagesCount']

            if remaining_message_count > 0:
                print('\n------------------\n')
