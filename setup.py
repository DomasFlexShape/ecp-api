from setuptools import setup

setup(
    name='ecp-api',
    packages=['ECP'],
    install_requires=[
        "requests", "zeep"
    ]
)